; Installer for Heat 5.05k

[Setup]
AppName=Heat505k
AppVersion=505k
WizardStyle=modern
DefaultDirName={autopf}\Heat505k
DefaultGroupName=Heat
Compression=lzma2
SolidCompression=yes
OutputDir=userdocs:Heat

[Files]
Source: "dist\Heat505.jar"; DestDir: "{app}"

[Icons]
Name: "{group}\Heat505k"; Filename: "{app}\Heat505.jar"
