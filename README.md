# Project Description

HEAT (Haskell Educational Advancement Tool) is a Haskell integrated development environment which is easy to use and suitable for students and the classroom. It was initially developed by [University of Kent](https://www.cs.kent.ac.uk/projects/heat/) and maintained until 2013.

# Installation

For Windows please download the Heat505 Installer from [Downloads](https://bitbucket.org/comprodev/heat/downloads/) and follow instructions. It comes with a private JRE9 distribution which is necessary for a proper displaying on High DPI screens.

Take into account that you also need to install the GHC compiler in your machine. You can download it from [https://www.haskell.org/platform/](https://www.haskell.org/platform/)

[//]: # (sdfadfalsdafsdfasdd4)
[//]: # (### Windows 10 Creators Update ###)
[//]: # (This update breaks all recent versions of GHC including 8.0.2. At the time of trying to start the GHCi compiler it shows a message like "the application was unable to start correctly (0xc0000142)".)
[//]: # (If you have any recent version until 8.0.2 included you need to follow this steps:)
[//]: # (* Download and extract the corresponding (i.e., i386 or x64) versions of the patched tar from [https://downloads.haskell.org/~ghc/8.0.2/](https://downloads.haskell.org/~ghc/8.0.2/). Be sure to get the versions with win10 in the name.)
[//]: # (* Replace the following directories in the Haskell install directory (typically C:\Program Files\Haskell Platform) with the corresponding directories in the tar: 8.0.2\bin, 8.0.2\mingw\bin, and 8.0.2\mingw\x86_64-w64-mingw32\bin (the latter is for 64-bit installs and will probably have a slightly different name for 32-bit installs).)
[//]: # (If you already have version 8.0.2-a or greater then you do not need to do anything else.)
[//]: # (Please note: If Haskell 8.0.2-a does not work after installing Windows 10 Creators Update then uninstall and reinstall Haskell.)

# Configuration

1. Click on Program > Options
![config01](config01.png)

2. Locate ghci.exe in your Haskell installation, leave other fields like they are by default. You can also skip Property Tests tab.

3. Optionally, if you want to modify the font size of the Editor and/or Interpreter to a more convenient size, go to Font Sizes tab, modify the values and apply changes.
![config02](config02.png)


# Implementation (for Heat developers)

You can always download, build and run Heat on any major platform since it is implemented in Java. If you are doing it in any IDE other than NetBeans you do not need the [nbproject/](nbproject/) folder. Additionally, remember that Java 9 is needed to run it properly on High DPI screens.

[Launch4J](http://launch4j.sourceforge.net/) was used to wrap the generated jar file into an exe file to call the private JRE9 and [Inno Setup Compiler](http://www.jrsoftware.org/isinfo.php) was used to build the Windows installer.

Find a summary of the latest fixes/upgrades to the source code in [changes.txt](changes.txt) and more documentation at the [HEAT](https://www.cs.kent.ac.uk/projects/heat/) original website.

### Relationship between MUM and [University of Kent](https://www.cs.kent.ac.uk/projects/heat/) branches ###

Our branch includes all the features existent in the most recent version from [University of Kent](https://www.cs.kent.ac.uk/projects/heat/) (v 5.05). Some of the latest upgrades include: Syntax highlighting and matching brackets, status of compilation, prompt and errors highlighting, program overview in a tree structure, and automatic checking of all properties of a program.

On the other hand, the MUM branch incorporates the next fixes and upgrades: Opening of .lhs files, recently opened file list on the menu, better default font size, some minor bug fixes, a Windows installer for High DPI screens and W10 Creators Update, and saving and reusing the size and position of the overall window.

### Original Authors from [University of Kent](https://www.cs.kent.ac.uk/projects/heat/): ###

| Version | Author               | Role                       |
| --------|----------------------|--------------------------- |
| HEAT 5  | Olaf Chitil          | System Architect & Coding  |
| HEAT 3  | Olaf Chitil          | System Architect & Coding  |
|         | Ivan Ivanovski       | Coding                     |
| HEAT 2  | Sergei Krot          | System Architect & Coding  |
|         | Jerome Gedge         | Coding                     |
|         | Stefanos Katsantonis | Quality assurance          |
|         | Danya Nusseir        | Project manager & Coding   |
| HEAT 1  | Dean Ashton          | System Architect & Coder   |
|         | Chris Olive          | Project Manager & Coder    |
|         | John Travers         | Quality Assurance & Coder  |
|         | Louis Whest          | Quality Assurance & Coder  |
