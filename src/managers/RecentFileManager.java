/**
 *
 * Copyright (c) 2005 University of Kent
 * Computing Laboratory, Canterbury, Kent, CT2 7NP, U.K
 *
 * This software is the confidential and proprietary information of the
 * Computing Laboratory of the University of Kent ("Confidential Information").
 * You shall not disclose such confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered into with
 * the University.
 *
 * @author Sergio Muriel (M.U.M.)
 * 
 */

package managers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * Manager for the INI file and Recent Files structure.
 * Recent file paths are stored in Heat505.ini
 */
public class RecentFileManager {
    
  private static final Logger log = Logger.getLogger("heat");

  private static RecentFileManager instance = null;
  
  private static final File iniFile = new File(System.getProperty("user.home") + "/Heat505.ini");
  
  private static final LinkedList<JMenuItem> entries = new LinkedList<>();
  
  private static final ActionManager am = ActionManager.getInstance();
  
  private static final WindowManager wm = WindowManager.getInstance();
  
  public static final int MAX_RECENT_ITEMS = 10;
  
  private RecentFileManager() {
    /* Exists to prevent instantiation */
      readEntries();
  }
  
  /**
   * Returns an instance of RecentFileManager
   *
   * @return RecentFileManager instance
   */
  public static RecentFileManager getInstance() {
    if (instance == null)
      instance = new RecentFileManager();
    return instance;
  }

  /**
   * Reads file from filesystem using a BufferedReader.
   * @return List<JMenuItem> the list of 
   */
  public JMenuItem[] getEntries() {
      JMenuItem[] entriesArr = new JMenuItem[entries.size()];
      entriesArr = entries.toArray(entriesArr);
      return entriesArr;
  }
  
  /**
   * Puts the most recent opened file entry first
   * @param file
   */
  public void updateEntries(File file) {
      if (entries.size() > 0) {
        File firstFile = ((ActionManager.OpenRecentFileAction) entries.getFirst().getAction()).getRecentFile();
        // No need to do anything if the chosen recent file was the first one.
        if (!firstFile.equals(file)) {
          entries.getFirst();
          JMenu menuFile = wm.getMainMenu().getjMenuFile();
          for (JMenuItem menuItem : entries) {
              menuFile.remove(menuItem);
          }
          boolean newEntry = true;
          int index = 0;
          JMenuItem menuItem = null;
          while (index < entries.size()) {
              menuItem = entries.get(index);
              ActionManager.OpenRecentFileAction action  = (ActionManager.OpenRecentFileAction )menuItem.getAction();
              if (action.getRecentFile().equals(file)) {
                  newEntry = false;
                  break;
              }
              index++;
          }
          if (newEntry) {
              menuItem =  new JMenuItem();
              ActionManager.OpenRecentFileAction rf = null;
              try {
                  rf = am.new OpenRecentFileAction(file);
              } catch (IOException ex) {
                  Logger.getLogger(RecentFileManager.class.getName()).log(Level.SEVERE, null, ex);
              }
              menuItem.setAction(rf);
              entries.addFirst(menuItem);
          } else {
              entries.remove(index);
              entries.addFirst(menuItem);
          }
          if (entries.size() > MAX_RECENT_ITEMS) {
              entries.removeLast();
          }
          for (JMenuItem mi : entries) {
              menuFile.add(mi);
          }
          saveEntries();
        }
      } else {
            JMenuItem menuItem =  new JMenuItem();
            ActionManager.OpenRecentFileAction rf = null;
            try {
                rf = am.new OpenRecentFileAction(file);
            } catch (IOException ex) {
                Logger.getLogger(RecentFileManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            menuItem.setAction(rf);
            entries.addFirst(menuItem);
            wm.getMainMenu().getjMenuFile().add(menuItem);
            saveEntries();
      }
  }
  
  /**
   * Reads and creates the entries from Heat505.ini.
   */
  @SuppressWarnings("empty-statement")
  private void readEntries() {
    BufferedReader input = null;
    try {
      input = new BufferedReader(new FileReader(iniFile));
      String line;
      int counter = 0;
      while (counter < MAX_RECENT_ITEMS && (line = input.readLine()) != null) {
        if (!line.equals("")) {
            JMenuItem menuItem =  new JMenuItem();
            File file = new File(line);
            ActionManager.OpenRecentFileAction rf = am.new OpenRecentFileAction(file);
            menuItem.setAction(rf);
            entries.addLast(menuItem);
            counter++;
        }
      }
    } catch (IOException ex) {
      log.log(Level.WARNING, "[RecentFileManager]: readFile failed {0}", iniFile.getAbsolutePath());
    } finally {
      try {
        if (input != null)
          input.close();
      } catch (IOException ex) {
        ex.printStackTrace();
      }
    }
  }
    
  /**
   * Writes the entries to Heat505.ini.
   */
  private void saveEntries() {
    StringBuilder content = new StringBuilder();
    for (JMenuItem entry : entries) {
        ActionManager.OpenRecentFileAction action  = (ActionManager.OpenRecentFileAction )entry.getAction();
        try {
            content.append(action.getRecentFile().getCanonicalPath());
        } catch (IOException ex) {
            Logger.getLogger(RecentFileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        content.append(System.getProperty("line.separator"));
    }
    BufferedWriter output = null;
    try {
      output = new BufferedWriter(new FileWriter(iniFile));
      output.write(content.toString());
    } catch (IOException ex) {
      log.log(Level.WARNING, "[RecentFileManager]: writeFile failed {0}", ex.getMessage());
    } finally {
      try {
        if (output != null)
          output.close();
      } catch (IOException ex) {
      }
    }
  }
    
}
